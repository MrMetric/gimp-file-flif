#!python3
import os

gimpver = ARGUMENTS.get('gimpver', '')

WARNING_FLAGS = [
	'-Weverything',
	'-Werror=cast-qual',
	'-Werror=conversion',
	'-Werror=delete-incomplete',
	'-Werror=delete-non-virtual-dtor',
	'-Werror=deprecated',
	'-Werror=extra-semi',
	'-Werror=extra-tokens',
	'-Werror=float-conversion',
	'-Werror=implicit-fallthrough',
	'-Werror=inconsistent-missing-destructor-override',
	'-Werror=inconsistent-missing-override',
	'-Werror=invalid-pp-token',
	'-Werror=mismatched-new-delete',
	'-Werror=mismatched-tags',
	'-Werror=missing-declarations',
	'-Werror=missing-field-initializers',
	'-Werror=missing-prototypes',
	'-Werror=multichar',
	'-Werror=newline-eof',
	'-Werror=non-virtual-dtor',
	'-Werror=old-style-cast',
	'-Werror=return-type',
	'-Werror=self-move',
	'-Werror=shorten-64-to-32',
	'-Werror=sign-compare',
	'-Werror=sign-conversion',
	'-Werror=static-inline-explicit-instantiation', # note: GCC says that `inline template<>` is invalid syntax
	'-Werror=string-compare',
	'-Werror=strict-prototypes',
	'-Werror=string-compare',
	'-Werror=string-plus-int',
	'-Werror=uninitialized',
	'-Werror=unknown-pragmas',
	'-Werror=unknown-warning-option',
	'-Werror=unused-result',
	'-Werror=weak-vtables',
	'-Werror=zero-as-null-pointer-constant',
	'-Wno-c++98-compat',
	'-Wno-c++98-compat-pedantic',
	'-Wno-comment',
	'-Wno-covered-switch-default',
	'-Wno-deprecated-dynamic-exception-spec',
	'-Wno-documentation-unknown-command',
	'-Wno-double-promotion',
	'-Wno-exit-time-destructors',
	'-Wno-float-equal',
	'-Wno-global-constructors',
	'-Wno-logical-op-parentheses',
	'-Wno-missing-braces',
	'-Wno-padded',
	'-Wno-shadow',
	'-Wno-undefined-func-template',
]

FSANITIZE = [
	'-fsanitize=address,undefined,unsigned-integer-overflow,nullability',
	'-fno-sanitize-recover=null',
]

CXXFLAGS = [
	'-std=c++17',
	'-iquote', 'src',
	'-isystem', 'lib',

	'-g',
	'-O0',

	'-fno-omit-frame-pointer',
	'-fstack-protector-strong',
	'-fvisibility=hidden',
	'-DVISIBLE=\'__attribute__((visibility("default")))\'',
]
CXXFLAGS += FSANITIZE
CXXFLAGS += WARNING_FLAGS

LINKFLAGS = [
	'-flto',
]
LINKFLAGS += FSANITIZE

LIBS = [
	'flif',
]

env = Environment()
env['ENV']['TERM'] = os.environ['TERM']
env['CC'] = 'clang'
env['CXX'] = 'clang++'
env.Append(CXXFLAGS = CXXFLAGS)
env.Append(LINKFLAGS = LINKFLAGS)
env.Append(LIBS = LIBS)
gimptool_name = 'gimptool'
if gimpver != '':
	gimptool_name += '-' + gimpver
env.ParseConfig(gimptool_name + ' --cflags --libs')

def get_src(directory):
	src = []
	for root, subdirs, files in os.walk(directory):
		for file in files:
			if file.endswith('.cpp'):
				src.append(os.path.join(root, file))
	return src
sources = get_src('src')
target = env.Program(target='file-flif', source=sources)
Default(target)
