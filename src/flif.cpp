#include "flif.hpp"

#include <cstring>

#define G_LOG_DOMAIN nullptr
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <libgimp/gimp.h>
#pragma clang diagnostic pop

#include "flif-load.hpp"

static void query();
static void run
(
	const gchar*     name,
	gint             nparams,
	const GimpParam* param,
	gint*            nreturn_vals,
	GimpParam**      return_vals
);

static GimpPlugInInfo PLUG_IN_INFO
{
	nullptr,
	nullptr,
	query,
	run,
};

MAIN()

static void query()
{
	static const GimpParamDef load_args[]
	{
		{ GIMP_PDB_INT32 , "run-mode"    , "The run mode" },
		{ GIMP_PDB_STRING, "filename"    , "The name of the file to load" },
		{ GIMP_PDB_STRING, "raw-filename", "The name as entered by the user" },
	};
	static const GimpParamDef load_return_vals[]
	{
		{ GIMP_PDB_IMAGE, "image", "The output image" },
	};

	gimp_install_procedure
	(
		PROC_LOAD,
		"Loads FLIF files",
		"Loads FLIF files (Free Lossless Image Format)",
		"Mr. Metric",
		"Mr. Metric",
		"2018",
		/*N_(*/"FLIF image"/*)*/,
		nullptr,
		GIMP_PLUGIN,
		G_N_ELEMENTS(load_args),
		G_N_ELEMENTS(load_return_vals),
		load_args,
		load_return_vals
	);
	gimp_register_file_handler_mime(PROC_LOAD, "image/flif");
	gimp_register_file_handler_mime(PROC_LOAD, "image/x-flif");
	gimp_register_magic_load_handler(PROC_LOAD, "flif", "", "0,string,FLIF");
}

static void run
(
	const gchar* const     name,
	const gint             nparams,
	const GimpParam* const param,
	gint* const            nreturn_vals,
	GimpParam** const      return_vals
)
{
	static GimpParam  values[2];
	GimpPDBStatusType status = GIMP_PDB_EXECUTION_ERROR;
	GError*           error = nullptr;

	//INIT_I18N();
	gegl_init(nullptr, nullptr);

	*nreturn_vals = 1;
	*return_vals  = values;

	values[0].type = GIMP_PDB_STATUS;

	const GimpRunMode run_mode = static_cast<GimpRunMode>(param[0].data.d_int32);

	if(std::strcmp(name, PROC_LOAD) == 0)
	{
		if(run_mode == GIMP_RUN_NONINTERACTIVE && nparams != 3)
		{
			status = GIMP_PDB_CALLING_ERROR;
		}
		else
		{
			gint32 image = load_image(param[1].data.d_string, error);
			if(image != -1)
			{
				*nreturn_vals = 2;
				values[1].type         = GIMP_PDB_IMAGE;
				values[1].data.d_image = image;
				status = GIMP_PDB_SUCCESS;
			}
			else
			{
				status = GIMP_PDB_EXECUTION_ERROR;
			}
		}
	}
	else
	{
		status = GIMP_PDB_CALLING_ERROR;
	}

	if(status != GIMP_PDB_SUCCESS && error != nullptr)
	{
		*nreturn_vals = 2;
		values[1].type          = GIMP_PDB_STRING;
		values[1].data.d_string = error->message;
	}

	values[0].data.d_status = status;
}
