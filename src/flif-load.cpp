#include <fstream>
#include <iostream>
#include <memory>
#include <stdint.h>

#define G_LOG_DOMAIN nullptr
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#pragma clang diagnostic pop

#include "flif-load.hpp"
#include "flif.hpp"

#include <FLIF/flif_dec.h>

using std::unique_ptr;

static void flif_image_read_row
(
	uint8_t n_channels,
	uint8_t n_bits,
	bool has_palette,
	FLIF_IMAGE* image,
	uint32_t y,
	uint8_t* pixels,
	size_t rowsize
);

gint32 load_image(const gchar* filename, GError*& error)
{
	unique_ptr<FLIF_DECODER, void (*)(FLIF_DECODER*)> fdec
	(
		flif_create_decoder(),
		flif_destroy_decoder
	);

	// TODO: check if these are the defaults
	flif_decoder_set_quality(fdec.get(), 100);
	flif_decoder_set_scale(fdec.get(), 1);


	gimp_progress_init_printf(/*_(*/"Reading '%s'"/*)*/, gimp_filename_to_utf8(filename));

	std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
	if(!in.is_open())
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"Unable to open file"/*)*/
		);
		return -1;
	}
	const size_t file_size = static_cast<size_t>(in.tellg());
	if(file_size == 0)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"The file is empty"/*)*/
		);
		return -1;
	}
	in.seekg(0);
	const unique_ptr<uint8_t[]> file_data = std::make_unique<uint8_t[]>(file_size);
	in.read(reinterpret_cast<char*>(file_data.get()), static_cast<std::streamsize>(file_size));


	gimp_progress_set_text_printf(/*_(*/"Decoding '%s'"/*)*/, gimp_filename_to_utf8(filename));

	unique_ptr<FLIF_INFO, void (*)(FLIF_INFO*)> finfo
	(
		flif_read_info_from_memory(file_data.get(), file_size),
		flif_destroy_info
	);
	if(finfo == nullptr)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"The image is invalid"/*)*/
		);
		return -1;
	}

	const uint32_t size_x = flif_info_get_width(finfo.get());
	const uint32_t size_y = flif_info_get_height(finfo.get());
	if(size_x > GIMP_MAX_IMAGE_SIZE || size_y > GIMP_MAX_IMAGE_SIZE)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"The image is too large: %u×%u"/*)*/,
			size_x, size_y
		);
		return -1;
	}

	const uint8_t n_channels = flif_info_get_nb_channels(finfo.get());
	if(n_channels == 0 || n_channels == 2 || n_channels > 4)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"The image has a bad amount of channels: %u"/*)*/,
			n_channels
		);
		return -1;
	}

	const uint8_t n_bits = flif_info_get_depth(finfo.get());
	if(n_bits != 8 && n_bits != 16)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"The image has an unsupported precision: %u-bit"/*)*/,
			n_bits
		);
		return -1;
	}

	const size_t n_frames = flif_info_num_images(finfo.get());
	if(n_frames != 1)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"Animated images are not supported (frames: %lu)"/*)*/,
			n_frames
		);
		return -1;
	}

	finfo = nullptr;

	if(!flif_decoder_decode_file(fdec.get(), filename))
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"Unable to decode the image"/*)*/
		);
		return -1;
	}

	FLIF_IMAGE* fimage = flif_decoder_get_image(fdec.get(), 0);
	if(fimage == nullptr)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"No image found"/*)*/
		);
		return -1;
	}

	const uint32_t palette_size = flif_image_get_palette_size(fimage);
	const bool has_palette = palette_size != 0;
	unique_ptr<uint8_t[]> palette;
	if(has_palette)
	{
		if(n_channels != 3 && n_channels != 4)
		{
			g_set_error
			(
				&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
				/*_(*/"Invalid amount of channels for paletted image: %u"/*)*/,
				n_channels
			);
			return -1;
		}
		if(n_bits != 8)
		{
			g_set_error
			(
				&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
				/*_(*/"Invalid precision for paletted image: %u-bit"/*)*/,
				n_bits
			);
			return -1;
		}

		palette = std::make_unique<uint8_t[]>(4*palette_size);
		flif_image_get_palette(fimage, palette.get());

		// delete the A component
		// TODO: find out how to send alpha values to The GIMP
		for(size_t i = 3; i < 3*palette_size; ++i)
		{
			palette[i] = palette[i + i / 3];
		}
	}

	const gint gsize_x = static_cast<gint>(size_x);
	const gint gsize_y = static_cast<gint>(size_y);
	GimpImageBaseType base_type;
	if(has_palette)
	{
		base_type = GIMP_INDEXED;
	}
	else
	{
		base_type = n_channels == 1 ? GIMP_GRAY : GIMP_RGB;
	}

	GimpImageType image_type;
	if(has_palette)
	{
		image_type = n_channels == 4 ? GIMP_INDEXEDA_IMAGE : GIMP_INDEXED_IMAGE;
	}
	else switch(n_channels)
	{
		case 1:
		{
			image_type = GIMP_GRAY_IMAGE;
			break;
		}
		// if FLIF had 2, would it be GIMP_GRAYA_IMAGE?
		case 3:
		{
			image_type = GIMP_RGB_IMAGE;
			break;
		}
		case 4:
		{
			image_type = GIMP_RGBA_IMAGE;
			break;
		}
	}

	const gint32 image = gimp_image_new_with_precision
	(
		gsize_x, gsize_y,
		base_type,
		// in 2.99: _GAMMA is deprecated, use _NON_LINEAR
		n_bits == 8 ? GIMP_PRECISION_U8_GAMMA : GIMP_PRECISION_U16_GAMMA
	);
	if(image == -1)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"Image creation failed"/*)*/
		);
		return -1;
	}

	gimp_image_set_filename(image, filename);

	if(has_palette)
	{
		gimp_image_set_colormap(image, palette.get(), static_cast<gint>(palette_size));
	}

	const gint32 layer = gimp_layer_new
	(
		image,
		/*_(*/"Background"/*)*/,
		gsize_x, gsize_y,
		image_type,
		100, // opacity %
		gimp_image_get_default_new_layer_mode(image)
	);
	if(layer == -1)
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"Layer creation failed"/*)*/
		);
		return -1;
	}

	if(!gimp_image_insert_layer(image, layer, -1, 0))
	{
		g_set_error
		(
			&error, G_FILE_ERROR, G_FILE_ERROR_FAILED,
			/*_(*/"Layer insertion failed"/*)*/
		);
		return -1;
	}

	const size_t rowsize = (has_palette ? 1 : n_channels) * (n_bits / 8) * size_x;
	unique_ptr<uint8_t[]> pixels = std::make_unique<uint8_t[]>(rowsize * size_x);
	double progress = 0;
	for(uint32_t y = 0; y < size_y; ++y)
	{
		flif_image_read_row(n_channels, n_bits, has_palette, fimage, y, pixels.get(), rowsize);

		const double new_progress = (static_cast<double>(y) + 1) / size_y;
		if(new_progress - progress >= 0.01)
		{
			progress = new_progress;
			gimp_progress_update(progress);
		}
	}

	unique_ptr<GeglBuffer, void (*)(gpointer)> buffer
	(
		gimp_drawable_get_buffer(layer),
		g_object_unref
	);
	const GeglRectangle rect
	{
		0, 0,
		gsize_x, gsize_y
	};
	gegl_buffer_set
	(
		buffer.get(),
		&rect,
		0, nullptr, // scale level, babl format
		pixels.get(),
		GEGL_AUTO_ROWSTRIDE
	);
	buffer = nullptr;
	pixels = nullptr;

	gimp_progress_update(1);

	return image;
}

static void flif_image_read_row
(
	const uint8_t n_channels,
	const uint8_t n_bits,
	const bool has_palette,
	FLIF_IMAGE* const image,
	const uint32_t y,
	uint8_t* const pixels,
	const size_t rowsize
)
{
	if(has_palette)
	{
		flif_image_read_row_PALETTE8(image, y, pixels + y*rowsize, rowsize);
		return;
	}

	// There are no RGB8 or RGB16 functions
	if(n_channels == 3)
	{
		const size_t rowsize2 = rowsize / 3 * 4;
		unique_ptr<uint8_t[]> buf2 = std::make_unique<uint8_t[]>(rowsize2);
		(n_bits == 8 ? flif_image_read_row_RGBA8 : flif_image_read_row_RGBA16)
		(image, y, buf2.get(), rowsize2);

		uint8_t* buf = pixels + y*rowsize;
		const uint8_t n_bytes = n_bits / 8;
		for(size_t i = 0; i < rowsize; ++i)
		{
			// I saved the graph 3 times
			// bad : https://www.desmos.com/calculator/w5vwosvw1s
			// okay: https://www.desmos.com/calculator/mumzzetw2f
			// good: https://www.desmos.com/calculator/x6mqqcp2gf
			buf[i] = buf2[i + n_bytes * (i / (3 * n_bytes))];
		}

		return;
	}

	(n_channels == 1 ? (n_bits == 8 ? flif_image_read_row_GRAY8 : flif_image_read_row_GRAY16)
	  : (n_bits == 8 ? flif_image_read_row_RGBA8 : flif_image_read_row_RGBA16))
	(image, y, pixels + y*rowsize, rowsize);
}
